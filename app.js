var express = require('express');
var path = require('path');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users-route.js');
var homeRouter = require('./routes/home-route')
const db =require('./models')
var app = express();

//Create DB
function syncDB(){
  db.sequelize.sync({ alter: true })
}
syncDB()

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api', usersRouter);
app.use('/api/task',homeRouter)

app.listen(3000)
module.exports = app;
