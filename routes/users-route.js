var express = require('express');
var router = express.Router();
const { UserController } = require('../controllers/users-controller')
const userFn = new UserController()

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

//user register
router.post('/users/register', (req, res) => {
  userFn.register(req, res)
})

//user login
router.post('/users/login', (req, res) => {
  userFn.login(req, res)
})

//user identify
router.post('/users/identify', (req, res) => {
  userFn.identify(req, res)
})

//user reset password
router.post('/users/resetpass', (req, res) => {
  userFn.resetPass(req, res)
})

module.exports = router;
