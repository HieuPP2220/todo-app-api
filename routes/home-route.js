var express = require('express');
var router = express.Router();
const { homeController } = require('../controllers/home-controller')
const homeFn = new homeController();



//add task
router.post('/', (req, res) => {
  homeFn.add(req, res)
})

//display task
router.get('/', (req, res) => {
  homeFn.display(req, res)
})


//edit task
router.put('/', (req, res) => {
  homeFn.edit(req, res)
})

//delete task
router.delete('/', (req, res) => {
  homeFn.delete(req, res)
})

//detail
router.get('/detail', (req, res) => {
  homeFn.detail(req, res)
})

//list task complete
router.get('/complete', (req, res) => {
  homeFn.completeTask(req, res)
})


module.exports = router;