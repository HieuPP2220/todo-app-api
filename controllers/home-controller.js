const { Task } = require('../models');

class homeController {
    constructor() {

    }

    //load data from DB
    async loadData() {
        const list = await Task.findAll()
        return list
    }

    //send response
    sendRes(check, data, mesSuccess, mesFail, res) {

        try {
            if (check != null) {
                res.send({
                    "status": 200,
                    "message": mesSuccess,
                    "data": { data }
                })
            }
            else
                res.send({
                    "status": 500,
                    "message": mesFail,
                    "data": null
                })
        }
        catch (e) {
            console.log('Error happend while connecting to the server: ', e)
        }
    }
    
    //add task
    async add(req, res) {
        const data = await this.loadData();
        const task = {
            title: req.body.title,
            description: req.body.description,
            exp: req.body.exp,
            notification: req.body.notification,
            status: req.body.status,
            userId: req.body.accessToken
        }
        let checkTitle;

        for (let i = 0; i < data.length; i++) {
            if (task.title == data[i].title)
                checkTitle = true;
        }
        if (!checkTitle) await Task.create(task);
        this.sendRes(checkTitle, null, "task exist", "add task success", res)
    }

    //display all task
    async display(req, res) {
        const data = await this.loadData();
        this.sendRes(data, data, "display success", "server error", res)
    }

    //edit task
    async edit(req, res) {
        const data = await this.loadData();
        const editStatus = await Task.update({ description: req.body.description, exp: req.body.exp, notification: req.body.notification, status: req.body.status }, {
            where: {
                title: req.body.title
            }
        });
        this.sendRes(editStatus, null, "edit task success", "server error", res)
    }

    //delete task
    async delete(req, res) {

        const result = await Task.destroy({
            where: {
                id: parseInt(req.body.id),
                userId: parseInt(req.body.accessToken)
            }
        });
        this.sendRes(result, null, "delete task success", "id not exist", res)
    }

    //detail
    async detail(req, res) {
        const task = await Task.findOne({ where: { id: req.body.id } });
        this.sendRes(task, task, "OK", "id not exist", res)
    }

    //list task complete
    async completeTask(req, res) {
        const data = await this.loadData();
        let list = [];

        for (let i = 0; i < data.length; i++) {
            if (data[i].status == 'done')
                list.push(data[i]);
        }
        this.sendRes(list, list, "OK", "list not exist", res)
    }

}


module.exports.homeController = homeController