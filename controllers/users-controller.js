const { User } = require('../models');

const nodemailer = require("nodemailer");



class UserController {

  //loadDateFromDB
  async loadData() {
    const list = await User.findAll()
    return list
  }

  //sendMail
  async sendMail(codeVerify, req) {

    //let testAccount = await nodemailer.createTestAccount();

    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'hieuphamdtu2220@gmail.com', // generated ethereal user
        pass: 'mylifeisgood2220', // generated ethereal password
      },
    });

    let info = await transporter.sendMail({
      from: '"Hieu Pham 👻" <hieuphamdtu2220@gmail.com>', // sender address
      to: req.body.email.toString(), // list of receivers
      subject: "Verify ✔", // Subject line
      text: codeVerify.toString(), // plain text body
    });
  }

  //send response
  sendRes(check, data, mesSuccess, mesFail, res) {

    try {
      if (check != null) {
        res.send({
          "status": 200,
          "message": mesSuccess,
          "data": { data }
        })
      }
      else
        res.send({
          "status": 500,
          "message": mesFail,
          "data": null
        })
    }
    catch (e) {
      console.log('Error happend while connecting to the server: ', e)
    }
  }
  //register
  async register(req, res) {
    const data = await this.loadData()
    let checkEmail = true;

    for (let i = 0; i < data.length; i++) {
      if (req.body.email == data[i].dataValues.email)
        checkEmail = false;
    }
    let user = {}
    if (checkEmail) {
      let accessToken
      let checkAccess = true;

      while (checkAccess) {
        let data = await this.loadData()
        accessToken = Math.floor(Math.random() * 10000) + 1
        for (let i = 0; i < data.length; i++) {
          if (accessToken != data[i].accessToken) {
            checkAccess = false;
          }
        }
      }
      var base64Data = req.body.avatar.replace(/^data:image\/png;base64,/, "");
      var userImage =  "./images/"+accessToken.toString()+req.body.name.toString()+".png";
      fs.writeFile(userImage, base64Data, 'base64');
      user = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        avatar: userImage,
        accessToken: accessToken
      }
      await User.create(user);
    console.log(user)
    this.sendRes(user, null, "register success", "email already exist", res)
    }else{
    this.sendRes(null, null, "register success", "email already exist", res)

    }
  }


  //login
  async login(req, res) {
    const user = await User.findOne({ where: { email: req.body.email, password: req.body.password } });
    const data = {
        accessToken : user.accessToken,
        email : user.email
    }
    this.sendRes(user, data, "Login success", "User or Password is incorrect", res)
  }

  //identify
  async identify(req, res) {
    const data = await this.loadData()
    let checkEmail;
    for (let i = 0; i < data.length; i++) {
      if (req.body.email == data[i].dataValues.email)
        checkEmail = true;
    }
    if (checkEmail) {
      const code = Math.floor(Math.random() * 1000000) + 1
      const verify = await this.sendMail(code, req)
    }
    this.sendRes(checkEmail, code, "verify success", "Email not exist", res)
  }

  //resetpass
  async resetPass(req, res) {
    const check = await User.update({ password: req.body.password }, {
      where: {
        email: req.body.email
      }
    });
    this.sendRes(check, null, "reset password success", "server error", res)
  }
}
module.exports.UserController = UserController